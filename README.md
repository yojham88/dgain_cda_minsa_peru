# ANÁLISIS DE GENERACIÓN DE UN DOCUMENTO CDA-HL7 EN DGAIN-MINSA

##### ENTIDAD:
>MINSA>DGAIN>DIPOS

##### Creator. yj

______________________________________________________________

### 1. ANTECEDENTES
- Para el cumplimiento del presente ejercicio técnico se revisó la siguiente documentación:
    * 1.1.	Norma Técnica de Salud Nº-139-2018, para la “Gestión de la Historia Clínica”, aprobada por Resolución Ministerial de Salud Nº-214 del 13 de marzo del año 2018 con modificatoria en Resolución Ministerial de Salud Nº-265 del mismo año.
    * 1.2.	Directiva Administrativa Nº-266 que regula la “Inter-operabilidad en los Sistemas de Información Asistenciales”, aprobada por Resolución Ministerial de Salud Nº-464 del 24 de mayo del año 2019.
    * 1.3.	Documento Técnico del "SIHCE del MINSA" aprobado por Resolución Ministerial de Salud Nº-080 del 14 de febrero del año 2022.
    * 1.4.	Directiva Administrativa Nº329 "Directiva Administrativa de Conformación de Redes Integradas de Salud-RIS"  aprobada en Resolución Ministerial de Salud N.°327 en año 2022
    * 1.5.	Health Level Seven International. (2022). Clinical Document Architecture (CDA). HL7 International. URL: http://www.hl7.org/implement/standards/product_brief.cfm?product_id=7.

### 2. ANÁLISIS DEL ESCENARIO
- La Dirección General de Aseguramiento e Intercambio Prestacional solicita al equipo técnico de Informática Médica verificar la arquitectura de datos clínicos (CDA) del estándar hl7v3 en base a la normatividad actual de Historia Clínica descrita en la NTS Nº-139-2018.

* #### 2.1.	Sobre los campos o variables identificadas en la Norma Técnica de Salud, citada en la referencia.
    * ##### 2.1.1. Se procede a identificar las variables listadas de manera linear y tabular en el documento de la norma técnica y se seleccionan las vinculadas directamente con el primer nivel de atención , donde se encuentra desplegándose los iniciadores RIS.
        - De 5.2.1. Formatos básicos
	        - 1. Formatos en consulta externa
		        - 1.1. En el primer nivel de atención (por etapas de vida)
                    - a, b y c diferidos por no guardar relación con campos de un registro.
                    - d. Documento: Atención integral de la niña y el niño
                        - Fecha y hora de la atención.
                        - N° de Historia Clínica.
                        - Datos Generales: Apellidos y Nombres del paciente o usuario de salud, sexo, edad, pertenencia étnica, fecha y lugar de nacimiento, procedencia, grado de instrucción, grupo sanguíneo y factor Rh, Nombre, edad y DNI de la madre, padre o acompañante.
                        - Antecedentes personales (antecedentes patológicos, quirúrgicos, laborales, uso de terapias alternativas/complementarias, estilos de vida).
                        - Antecedentes familiares.
                        - Esquema de vacunación.
                        - Vigilancia del crecimiento y desarrollo.
                        - Datos en triaje; signos vitales descarte de signos de alarma.
                        - Anamnesis: motivo de consulta, forma de inicio, tiempo de enfermedad.
                        - Preguntas sobre problemas frecuentes en la infancia.
                        - Evaluación de la alimentación actual.
                        - Examen físico.
                        - Diagnóstico (CIE 10), incluyendo diagnóstico nutricional.
                        - Tratamiento.
                        - Exámenes auxiliares.
                        - Referencia si fuera el caso.
                        - Fecha de próxima cita.
                        - Firma, sello y colegiatura del profesional que presta la atención.
                    - e. Documento: Atención integral del adolescente
                        - Fecha y hora de la atención.
                        - N° de Historia Clínica.
                        - Datos Generales: apellidos y nombres del paciente o usuario de salud, sexo, pertinencia étnica, edad, fecha de nacimiento, lugar de nacimiento, procedencia, grado de instrucción, centro educativo, estado civil, ocupación, grupo sanguíneo  y factor Rh, nombre, edad, DNI de la madre, padre o acompañante.
                        - Antecedentes personales (antecedentes patológicos, quirúrgicos, laborales, uso de terapias alternativas/complementarias, estilos de vida).
                        - Antecedentes familiares.
                        - Antecedentes psicosociales.
                        - Salud sexual y reproductiva.
                        - Salud bucal.
                        - Motivo de consulta.
                        - Tiempo de enfermedad.
                        - Funciones biológicas.
                        - Evaluación antropométrica.
                        - Evaluación de Riesgo cardiovascular.
                        - Funciones Vitales.
                        - Examen físico.
                        - Diagnóstico (CIE 10).
                        - Tratamiento.
                        - Exámenes auxiliares.
                        - Referencias si fuera el caso.
                        - Fecha de próxima cita.
                        - Firma, sello y colegiatura del profesional que presta la atención.
                    - f. Documento: Atención integral del joven
                        - Fecha y hora de la atención.
                        - N° de Historia Clínica.
                        - Datos Generales: apellidos y nombres, sexo del paciente o usuario de salud, pertinencia étnica, edad, fecha de nacimiento, lugar de nacimiento, procedencia, grado de instrucción, estado civil, ocupación, grupo sanguíneo y factor Rh, nombre, edad, DNI de la madre, padre o acompañante.
                        - Antecedentes personales (antecedentes patológicos, quirúrgicos, laborales, uso de terapias alternativas/complementarias, estilos de vida).
                        - Antecedentes familiares.
                        - Antecedentes psicosociales.
                        - Salud sexual y reproductiva.
                        - Salud bucal.
                        - Motivo de consulta.
                        - Tiempo de enfermedad.
                        - Funciones biológicas.
                        - Evaluación antropométrica.
                        - Evaluación de Riesgo cardiovascular.
                        - Funciones Vitales.
                        - Examen físico.
                        - Diagnóstico (CIE 10).
                        - Tratamiento.
                        - Exámenes auxiliares.
                        - Referencias si fuera el caso.
                        - Fecha de próxima cita.
                        - Firma, sello y colegiatura del profesional que presta la atención.
                    - g. Documento: Atención integral del adulto
                        - Fecha y hora de la atención.
                        - N° de Historia Clínica.
                        - Datos Generales: apellidos y nombres del paciente o usuario de salud, sexo, pertinencia étnica, edad, DNI, fecha de nacimiento, lugar de nacimiento, procedencia, grado de instrucción, centro educativo, estado civil, ocupación, grupo sanguíneo y factor Rh.
                        - Antecedentes personales (antecedentes patológicos, quirúrgicos, laborales, uso de terapias alternativas/ complementarias, estilos de vida).
                        - Antecedentes familiares.
                        - Alergia a medicamentos.
                        - Sexualidad.
                        - Salud bucal.
                        - Motivo de consulta.
                        - Tiempo de enfermedad.
                        - Funciones biológicas.
                        - Examen físico.
                        - Diagnóstico (CIE 10).
                        - Tratamiento.
                        - Exámenes auxiliares.
                        - Referencias si fuera el caso.
                        - Fecha de próxima cita.
                        - Firma, sello y colegiatura del profesional que presta la atención.
                    - h. Documento: Atención integral del adulto mayor
                        - Fecha y hora de la atención.
                        - N° de Historia Clínica.
                        - Datos Generales: apellidos y nombres del paciente o usuario de salud, sexo, pertinencia étnica, edad, DNI, fecha de nacimiento, lugar de nacimiento, procedencia, grado de instrucción, religión, estado civil, ocupación, grupo sanguíneo y factor Rh, nombre, edad, DNI y parentesco del familiar o cuidador responsable.
                        - Antecedentes personales (antecedentes patológicos, quirúrgicos, laborales, uso de terapias alternativas/complementarias, estilos de vida).
                        - Antecedentes familiares.
                        - Alergia a medicamentos.
                        - Medicamentos de uso frecuente.
                        - Reacción adversa a medicamentos.
                        - Valoración clínica del adulto mayor VACAM: valoración funcional, valoración mental, valoración socio familiar.
                        - Categorías del adulto mayor.
                        - Salud bucal.
                        - Motivo de consulta.
                        - Tiempo de enfermedad.
                        - Funciones biológicas.
                        - Examen físico.
                        - Diagnóstico: funcional, mental, socio familiar, físico.
                        - Tratamiento.
                        - Exámenes auxiliares.
                        - Referencias si fuera el caso.
                        - Fecha de próxima cita.
                        - Firma, sello y colegiatura del profesional que presta la atención.
        - De 5.2.2. Formatos especiales
            - Formatos de Filiación
                - Identificación Estándar de la Institución Prestadora de Servicios de Salud.
                - Categoría de la Institución Prestadora de Servicios de Salud.
                - Número de Historia Clínica.
                - Nombres y Apellidos del paciente o usuario de salud.
                - Lugar y fecha de nacimiento.
                - Edad.
                - Sexo.
                - Grupo Sanguíneo.
                - Factor Rh.
                - Domicilio actual.
                - Domicilio de procedencia.
                - Teléfono.
                - Documento de identificación (DNI, Carné de extranjería).
                - N° de Seguro Social, SIS, SOAT, otros.
                - Estado Civil.
                - Grado de Instrucción.
                - Ocupación.
                - Religión.
                - Nombre y DNI de la persona acompañante o responsable.
                - Domicilio de la persona acompañante o responsable.
    * ##### 2.1.2. Análisis de los formatos, estructurando sus variables en vista de tabla:
        - Se realiza el listado de los registros en  “vista de tabla” para identificar las duplicaciones que se generaran en los diferentes formatos mencionados en la norma y seleccionados en el item 2.1.1.
        - Tabla 01: Duplicidad de registros en diferentes documentos. 
        ![Duplicidad de registros](<imagen/dgain_duplicidad_registros.png>)
        - En base a las duplicidades identificadas, se realiza la liberación de los registros respectivos y se vinculan los “Datos generales” en un nuevo grupo de clasificación denominado “Datos Generales de Identificación” al cual también se suman las variables del formato de filiación.
        - Tabla 02: Liberación de doble registros con independencia del tipo de documento o formato solicitado.
        ![Libera duplicidad](<imagen/dgain_libera_registros.png>)
* #### 2.2. Cambio de enfoque en la generación de los registros, basado en el paradigma de programación orientado a objetos.
    Este modelo de interpretación, dara sustento a la reducción del tipo de registros y será el respaldo en la creación de documentos que aborden elementos de cada uno de los formatos.
    Gráfico 01: Sobre programación orientado a objetos. 
    ![Programación orientada a objetos](<imagen/programacion_orientada_objetos.jpg>)
* #### 2.3. Primer alcance para vinculación con descripciones del estándar hl7 en la documentación.
    En base a la información tecnica normativa emitida por nuestro organano de apoyo OGTI, donde se menciona los lineamientos de interoperatividad a seguir, y luego de estructurado los datos que requerimos, mencionados en el item anterior, se identifican los elementos más cercanos al tipo de campo/ registro de los formatos indicados en la norma.
    Tabla: Equivalencia de variables
    ![equivalencia_variables](<imagen/vinculacion_norma_minsa_variables_aleatorias_hl7.png>)
* #### 2.4. Busqueda de equivalencias en cuanto a elementos directos de un documento en CDA
    - Basados en lo analizado en el item anterior podemos encontrar equivalencias que logren satisfacer el dato requerido en la norma técnica de manera más directa y guardando relación a estructuras del documentos CDA.
    Tabla: Elementos de equivalencia en campos que contienen elementos de la arquitectura de documentos CDA.
    ![equivalencia_arquitectura](<imagen/equivalencias_cda_dgain.jpg>)
    - Tomando un ejemplo: 
        Si queremos identificar este registro:  recordTarget.patientRole.patient.birthplace.place,
        y obtener estos datos: “Michael Arque Lima Jesus Maria 10001 Peru”
        La serialización sería el siguiente:
            
            ```
            <recordTarget>
            <patientRole>
                <id root="2.16.840.1.113883.19.5" extension="12345"/>
                <patient>
                <name>
                    <given>Michael</given>
                    <family>Arque</family>
                </name>
                <birthTime value="19880101"/>
                <birthplace>
                    <place>
                    <addr>
                        <city>Jesus Maria</city>
                        <state>Lima</state>
                        <postalCode>10000</postalCode>
                        <country>PERU</country>
                    </addr>
                    </place>
                </birthplace>
                </patient>
            </patientRole>
            </recordTarget>
            ```
    * ##### 2.4.1. Identificación de elementos y su interpretación en segmentos de xml
        - A continuación se listarán diversos xml relacionados con la equivalencia descrita en el item anterior y de diversas secciones de los cuales forma parte los documentos CDA (los datos y los codeSystem son registros de prueba).
            ##### Información demográfica del paciente:
                ```
                <recordTarget>
                    <patientRole>
                        <!-- Número nacional identificación del paciente -->
                        <id root="1.2.840.113556.1.4.1" extension="1234567890"/>
                        <!-- Domicilio actual del paciente -->
                        <addr use="HP">
                            <streetAddressLine>123 Calle Principal</streetAddressLine>
                            <city>Quito</city>
                            <state>PI</state>
                            <postalCode>12345</postalCode>
                        </addr>
                        <!-- Teléfono de contacto del paciente -->
                        <telecom use="HP" value="tel:+1234567890"/>
                        <patient>
                            <!-- Apellidos y nombre del paciente -->
                            <name>
                                <given>Juan</given>
                                <family>Perez</family>
                            </name>
                            <!-- Sexo del paciente -->
                            <administrativeGenderCode code="M" codeSystem="2.16.840.1.113883.5.1"/>
                            <!-- Fecha de nacimiento del paciente -->
                            <birthTime value="19800101"/>
                            <!-- Lugar de nacimiento del paciente -->
                            <birthplace>
                                <addr>
                                    <city>Quito</city>
                                    <state>PI</state>
                                </addr>
                            </birthplace>
                            <!-- Edad del paciente (calculada basada en la fecha de nacimiento) -->
                            <!-- Grupo sanguíneo del paciente -->
                            <bloodGroupCode code="O" codeSystem="2.16.840.1.113883.5.2"/>
                            <!-- Factor Rh del paciente -->
                            <RhesusCode code="POS" codeSystem="2.16.840.1.113883.5.3"/>
                            <!-- Estado Civil del paciente -->
                            <maritalStatusCode code="M" codeSystem="2.16.840.1.113883.5.2"/>
                            <!-- Grado de instrucción educativa del paciente -->
                            <educationLevelCode code="UN" codeSystem="2.16.840.1.113883.5.60"/>
                            <!-- Centro educativo final del paciente (ejemplo: Universidad de Quito) -->
                            <!-- Ocupación del paciente -->
                            <occupation>
                                <code code="213" codeSystem="2.16.840.1.113883.5.4"/>
                                <jobTitle>Ingeniero de Software</jobTitle>
                            </occupation>
                            <!-- Religión del paciente -->
                            <religiousAffiliationCode code="101" codeSystem="2.16.840.1.113883.5.1076"/>
                            <!-- Pertenencia étnica del paciente -->
                            <raceCode code="2106-3" codeSystem="2.16.840.1.113883.5.104"/>
                        </patient>
                        <!-- Número de seguro social del paciente -->
                        <id root="2.16.840.1.113883.4.1" extension="123456789"/>
                    </patientRole>
                </recordTarget>
                ```
            ##### Antecedentes médicos del paciente:
                ```
                <section>
                    <title>Antecedentes Médicos</title>
                    <text>Detalles de los antecedentes médicos del paciente.</text>
                    <!-- Antecedentes de consumo frecuente de medicamentos -->
                    <entry>
                        <substanceAdministration classCode="SBADM">
                            <!-- Medicamento que el paciente toma con regularidad -->
                            <consumable>
                                <manufacturedProduct>
                                    <manufacturedMaterial>
                                        <code code="12345-6" codeSystem="2.16.840.1.113883.6.1"/>
                                        <name>Medicamento XYZ</name>
                                    </manufacturedMaterial>
                                </manufacturedProduct>
                            </consumable>
                        </substanceAdministration>
                    </entry>
                    <!-- Antecedentes de alergias del paciente -->
                    <entry>
                        <act classCode="ACT" moodCode="EVN">
                            <code code="420134006" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>Alérgico a ciertos alimentos.</text>
                        </act>
                    </entry>
                    <!-- Antecedentes quirúrgicos del paciente -->
                    <entry>
                        <procedure classCode="PROC">
                            <code code="123456" codeSystem="2.16.840.1.113883.6.12"/>
                            <text>El paciente se sometió a una cirugía de apendicitis en 2010.</text>
                        </procedure>
                    </entry>
                    <!-- Antecedentes de salud laboral del paciente -->
                    <entry>
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="50058000" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente trabaja en la construcción y ha tenido lesiones en el pasado.</text>
                        </observation>
                    </entry>
                    <!-- Antecedentes de uso de terapias alternativas del paciente -->
                    <entry>
                        <act classCode="ACT" moodCode="EVN">
                            <code code="88722007" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente ha utilizado acupuntura para tratar dolores crónicos.</text>
                        </act>
                    </entry>
                    <!-- Antecedentes de estilos de vida no saludables del paciente -->
                    <entry>
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="2827005" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente fuma y consume alcohol regularmente.</text>
                        </observation>
                    </entry>
                    <!-- Antecedentes patológicos familiares del paciente -->
                    <entry>
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="10198003" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>En la familia del paciente, hay antecedentes de enfermedades cardíacas.</text>
                        </observation>
                    </entry>
                    <!-- Antecedentes psicosociales del paciente -->
                    <entry>
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="72956006" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente ha estado bajo estrés debido a su trabajo.</text>
                        </observation>
                    </entry>
                </section>
                ```
            ##### Inmunizaciones:
                ```
                <section>
                    <title>Vacunas</title>
                    <text>Información sobre las vacunas administradas al paciente.</text>
                    <!-- Ejemplo de vacuna contra la gripe -->
                    <entry>
                        <substanceAdministration classCode="SBADM">
                            <consumable>
                                <manufacturedProduct>
                                    <manufacturedMaterial>
                                        <code code="123459" displayName="Vacuna contra la gripe" codeSystem="2.16.840.1.113883.6.88"/>
                                        <name>Flu-Vax</name>
                                    </manufacturedMaterial>
                                    <manufacturerOrganization>
                                        <name>Farmacia Saludable</name>
                                    </manufacturerOrganization>
                                </manufacturedProduct>
                            </consumable>
                            <text>El paciente recibió la vacuna Flu-Vax contra la gripe en septiembre de 2022.</text>
                        </substanceAdministration>
                    </entry>
                </section>
                ```
            ##### Hospitalizaciones anteriores:

                ```
                <section>
                    <title>Hospitalizaciones Anteriores</title>
                    <text>Registro de hospitalizaciones previas del paciente.</text>
                    <!-- Ejemplo de una hospitalización por apendicitis -->
                    <entry>
                        <encounter classCode="ENC">
                            <code code="32485007" displayName="Apendicitis" codeSystem="2.16.840.1.113883.6.96"/>
                            <effectiveTime>
                                <low value="20181201"/>
                                <high value="20181210"/>
                            </effectiveTime>
                            <location>
                                <healthCareFacility>
                                    <location>
                                        <name>Hospital Central</name>
                                        <addr>
                                            <streetAddressLine>Calle 123</streetAddressLine>
                                            <city>Metropolis</city>
                                            <state>EstadoX</state>
                                            <postalCode>12345</postalCode>
                                        </addr>
                                    </location>
                                </healthCareFacility>
                            </location>
                            <text>El paciente fue hospitalizado por apendicitis y sometido a una cirugía.</text>
                        </encounter>
                    </entry>
                    <!-- Otro ejemplo podría ser una hospitalización por un accidente, enfermedad cardíaca, etc. -->
                </section>
                ```
            ##### Historia Social:
                ```
                <section>
                    <title>Historia Social</title>
                    <text>Registro sobre hábitos y condiciones sociales del paciente.</text>
                    <!-- Ejemplo de hábito de fumar -->
                    <entry>
                        <observation classCode="OBS">
                            <value code="77176002" displayName="Fumador" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente ha sido fumador por 10 años pero dejó el hábito hace 3 años.</text>
                        </observation>
                    </entry>
                </section>
                ```
            ##### Registro de signos vitales:
                ```
                <section>
                    <title>Registro de Signos Vitales</title>
                    <text>Registro de los signos vitales del paciente en la última evaluación médica.</text>
                    <!-- Ejemplo de signos vitales -->
                    <entry>
                        <organizer classCode="CLUSTER">
                            <component>
                                <observation classCode="OBS">
                                    <code code="8480-6" displayName="Presión Sistólica" codeSystem="2.16.840.1.113883.6.1"/>
                                    <value xsi:type="PQ" value="120" unit="mm[Hg]"/>
                                </observation>
                            </component>
                            <component>
                                <observation classCode="OBS">
                                    <code code="8462-4" displayName="Frecuencia Cardíaca" codeSystem="2.16.840.1.113883.6.1"/>
                                    <value xsi:type="PQ" value="75" unit="/min"/>
                                </observation>
                            </component>
                            <component>
                                <observation classCode="OBS">
                                    <code code="9279-1" displayName="Temperatura Corporal" codeSystem="2.16.840.1.113883.6.1"/>
                                    <value xsi:type="PQ" value="36.7" unit="°C"/>
                                </observation>
                            </component>
                        </organizer>
                    </entry>
                </section>
                ```
            ##### Evaluación cognitiva:
                ```
                <section>
                    <title>Evaluación Cognitiva</title>
                    <text>Resultados sobre evaluaciones cognitivas.</text>
                    <!-- Ejemplo de evaluación de memoria -->
                    <entry>
                        <observation classCode="OBS">
                            <value code="418464007" displayName="Función de memoria normal" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente demostró una función de memoria dentro de los rangos normales en la última evaluación.</text>
                        </observation>
                    </entry>
                </section>
                ```
            ##### Encuentros médicos:
                ```
                <section>
                    <title>Encuentros Médicos</title>
                    <text>Detalles de las visitas o consultas médicas del paciente.</text>
                    <!-- Ejemplo: Visita al dermatólogo en 2021 -->
                    <entry>
                        <encounter classCode="ENC">
                            <code code="103187001" displayName="Consulta dermatológica" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente visitó al dermatólogo en septiembre de 2021 debido a una erupción cutánea.</text>
                        </encounter>
                    </entry>
                </section>
                ```
            ##### Resultados de laboratorio:
                ```
                <section>
                    <title>Resultados de Laboratorio</title>
                    <text>Registro de los resultados de pruebas de laboratorio.</text>
                    <!-- Ejemplo: Hemograma completo -->
                    <entry>
                        <observation classCode="OBS">
                            <code code="104325006" displayName="Hemograma completo" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>Resultados del hemograma completo realizado en junio de 2022. Hemoglobina: 15 g/dL, Hematocrito: 45%, Leucocitos: 7000/uL.</text>
                        </observation>
                    </entry>
                </section>
                ```
            ##### Evaluaciones y planes:
                ```
                <section>
                    <title>Evaluaciones y Planes</title>
                    <text>Evaluaciones realizadas y planes de tratamiento.</text>
                    <!-- Ejemplo: Evaluación de una lesión en la rodilla y plan de fisioterapia -->
                    <entry>
                        <act classCode="ACT">
                            <text>Lesión en la rodilla evaluada. Se propone un plan de 10 sesiones de fisioterapia.</text>
                        </act>
                    </entry>
                </section>
                ```
            ##### Dispositivos médicos:
                ```
                <section>
                    <title>Dispositivos Médicos</title>
                    <text>Dispositivos médicos que el paciente tiene implantados o usa regularmente.</text>
                    <!-- Ejemplo: Marcapasos colocado en 2019 -->
                    <entry>
                        <procedure classCode="PROC">
                            <code code="10370003" displayName="Implante de marcapasos" codeSystem="2.16.840.1.113883.6.96"/>
                            <text>El paciente recibió un marcapasos en julio de 2019.</text>
                        </procedure>
                    </entry>
                </section>
                ```
            - Y así sucesivamente se pueden citar las diferentes secciones en este tipo de serialización de datos.
* #### 2.5 Ejemplo de estructuración de modelo relacional

    Basados en la información anterior, los documentos CDA pueden contener una o más secciones que el diagrama de relaciones de los sistemas informaticos en salud definan para su operación y en concordancia con sus necesidades de información.
    A través de una equivalencia indirecta se logra estructurar código en java para generar una relación entre los diferentes documentos que formen parte de un sistema de registros en salud aleatorio (se toma como referncia java, ya que cuenta con un SDK de hl7 llamado HAPI)
    ##### Segmento 01:
        ```
        public class ClinicalDocument {
            private RecordTarget recordTarget;
            private List<Section> sections;
            // Otros atributos para el documento, como versiones, fechas, etc.
        }

        public class RecordTarget {
            private PatientRole patientRole;
            private String id;
        }

        public class PatientRole {
            private String id;
            private Address address;
            private String telecom;
            private ProviderOrganization providerOrganization;
            private PersonName patientName;
        }

        public class Address {
            private String streetAddress;
            private String city;
            private String state;
            private String postalCode;
            private String country;
        }

        public class ProviderOrganization {
            private String id;
            private String name;
            private Address address;
        }

        public class PersonName {
            private String firstName;
            private String middleName;
            private String lastName;
            private String prefix;  // Mr., Mrs., Dr., etc.
            private String suffix;  // Jr., III, etc.
        }

        public abstract class Section {
            private String title;
            private String code; // Código que identifica la sección (de algún código estándar)
            // otros atributos comunes a todas las secciones
        }

        public class PersonalDataSection extends Section {
            private Date birthdate;
            private String gender;
            private String maritalStatus;
            private String ethnicity;
            private String language;
            private String occupation;
        }

        public class MedicationSection extends Section {
            private List<Medication> medications;
        }

        public class Medication {
            private String medicationName;
            private String dosageInformation;
            private String route; // oral, intravenous, etc.
            private String frequency; 
            private Date startDate;
            private Date endDate;
            private String reason;
        }

        public class ChronicDiseasesSection extends Section {
            private List<ChronicDisease> diseases;
        }

        public class ChronicDisease {
            private String diseaseName;
            private Date diagnosisDate;
            private String status; // active, in remission, etc.
        }

        ```

    ##### Segmento 02:

        ```
        public class ImmunizationSection extends Section {
            private List<Immunization> immunizations;
        }

        public class Immunization {
            private String vaccineName;
            private Date administrationDate;
            private String dose;
        }

        public class AllergiesSection extends Section {
            private List<Allergy> allergies;
        }

        public class Allergy {
            private String allergen;
            private String reaction;
            private Date onsetDate;
            private String severity;
        }

        public class ProceduresSection extends Section {
            private List<Procedure> procedures;
        }

        public class Procedure {
            private String procedureName;
            private Date procedureDate;
            private String location;
            private String provider;
        }

        public class LabResultsSection extends Section {
            private List<LabResult> labResults;
        }

        public class LabResult {
            private String testName;
            private String result;
            private String unit;
            private String referenceRange;
            private Date resultDate;
        }

        public class RadiologySection extends Section {
            private List<RadiologyReport> reports;
        }

        public class RadiologyReport {
            private String studyName;
            private Date studyDate;
            private String findings;
            private String impressions;
        }

        ```

    ##### Segmento 03:

        ```
        public class VitalSignsSection extends Section {
            private List<VitalSign> vitalSigns;
        }

        public class VitalSign {
            private String type; // e.g. blood pressure, heart rate, temperature
            private String value;
            private Date measurementDate;
        }

        public class SocialHistorySection extends Section {
            private String tobaccoUse;
            private String alcoholUse;
            private String drugUse;
            // otros aspectos relevantes de la historia social
        }

        public class FamilyHistorySection extends Section {
            private List<FamilyMember> familyMembers;
        }

        public class FamilyMember {
            private String relationship;
            private String condition;
        }

        public class SurgicalHistorySection extends Section {
            private List<SurgicalProcedure> surgicalProcedures;
        }

        public class SurgicalProcedure {
            private String procedureName;
            private Date procedureDate;
            private String outcome;
            // otros atributos relevantes
        }
        ```

### 3. Generación de formato en CDA:

```
<ClinicalDocument xmlns="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <!-- Cabecera (Sección con información del documento) -->
    <typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
    <templateId root="1.2.3.4.5.6.7.8.9"/>
    <id root="2.16.840.1.113883.19.4" extension="12345"/>
    <code code="34133-9" displayName="Resumen de Historia Clínica" codeSystem="2.16.840.1.113883.6.1"/>
    <title>Resumen de Historia Clínica</title>
    <effectiveTime value="20231030"/>
    <!-- Paciente (Sección con información del paciente) -->
    <recordTarget>
        <patientRole>
            <id extension="987654321" root="2.16.840.1.113883.4.1"/>
            <patient>
                <name>
                    <given>Yon</given>
                    <family>Diaz</family>
                </name>
                <birthTime value="19950101"/>
                <administrativeGenderCode code="M" displayName="Masculino"/>
            </patient>
        </patientRole>
    </recordTarget>
    <!-- Historia Clínica (Número de Historia Clínica) -->
    <component>
        <structuredBody>
            <component>
                <section>
                    <code code="48765-2" displayName="Historia Clínica Número" codeSystem="2.16.840.1.113883.6.1"/>
                    <text>
                        <paragraph>1234567890</paragraph>
                    </text>
                </section>
            </component>
            <!-- Antecedentes (Antecedentes de enfermedades) -->
            <component>
                <section>
                    <code code="11348-0" displayName="Antecedentes de Enfermedades" codeSystem="2.16.840.1.113883.6.1"/>
                    <text>
                        <paragraph>El paciente tiene un historial de diabetes tipo 2 y hipertensión.</paragraph>
                    </text>
                </section>
            </component>
            <!-- Diagnóstico (Diagnóstico en CIE 10) -->
            <component>
                <section>
                    <code code="29308-4" displayName="Diagnóstico" codeSystem="2.16.840.1.113883.6.1"/>
                    <entry typeCode="DRIV">
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="E11.9" displayName="Diabetes tipo 2 sin complicaciones" codeSystem="2.16.840.1.113883.6.3"/>
                            <effectiveTime value="20231030"/>
                        </observation>
                    </entry>
                </section>
            </component>
            <!-- Resultados de Laboratorio -->
            <component>
                <section>
                    <code code="30954-2" displayName="Resultados de Laboratorio" codeSystem="2.16.840.1.113883.6.1"/>
                    <text>
                        <paragraph>Glucosa en ayunas: 150 mg/dL, Hemoglobina A1c: 7.5%</paragraph>
                    </text>
                </section>
            </component>
            <!-- Tratamiento -->
            <component>
                <section>
                    <code code="18776-5" displayName="Tratamiento" codeSystem="2.16.840.1.113883.6.1"/>
                    <text>
                        <paragraph>Metformina 500 mg diarios, Losartan 50 mg diarios</paragraph>
                    </text>
                </section>
            </component>
            <!-- Firma del Médico -->
            <component>
                <section>
                    <code code="18842-5" displayName="Firma del Médico" codeSystem="2.16.840.1.113883.6.1"/>
                    <text>
                        <paragraph>Dr. Alvarado Alvarado</paragraph>
                    </text>
                </section>
            </component>
        </structuredBody>
    </component>
    <!-- Firma Digital -->
    <authenticator>
        <time value="20231030"/>
        <signatureCode code="S"/>
        <assignedEntity>
            <id extension="54321" root="2.16.840.1.113883.19.5"/>
            <assignedPerson>
                <name>
                    <given>Alberto</given>
                    <family>Gómez</family>
                </name>
            </assignedPerson>
        </assignedEntity>
    </authenticator>
</ClinicalDocument>
```
***
###### Pendiente confirmación de verificación por el área TI
***


[def]: image.png